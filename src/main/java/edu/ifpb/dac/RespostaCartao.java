/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.ifpb.dac;

import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.ActivationConfigProperty;
import javax.ejb.MessageDriven;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;

//@MessageDriven(mappedName = "java:global/jms/outQueue")
@MessageDriven(
        mappedName = "java:global/jms/outQueue",
        activationConfig = {
            @ActivationConfigProperty(propertyName = "destinationType",
                    propertyValue = "javax.jms.Queue"),
        })

public class RespostaCartao implements MessageListener {
 
    static String resposta;
       
    @Override
    public void onMessage(Message message) {
        try {
            TextMessage tm = (TextMessage) message;
            resposta = tm.getText();
            System.out.println("Resposta do Servidor Cartão: Pagamento "+ resposta);
        } catch (JMSException ex) {
            Logger.getLogger(RespostaCartao.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
