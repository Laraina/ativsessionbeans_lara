
package edu.ifpb.dac;

import java.util.List;
import javax.jms.JMSException;

public interface Carrinho {

    public void add(Produto produto);
    
    public void del(Produto produto);

    public int finalizar(Pedido p);
    
     public void cancelar();
    
    public List<Produto> listaDeProdutos();
    
    public void salvarPedido(Pedido p);
    
   public String retornaResposta() throws JMSException;

}
